package com.maulida.pinsarpras.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maulida.pinsarpras.Model.Jurusan
import com.maulida.pinsarpras.R
import com.maulida.pinsarpras.databinding.ItemJurusanBinding
import com.squareup.picasso.Picasso

class JurusanAdapter(c: Context) : RecyclerView.Adapter<JurusanAdapter.JurusanViewHolder>() {
    private var context: Context = c
    private var arrlist: List<Jurusan> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JurusanAdapter.JurusanViewHolder {
        val binding = ItemJurusanBinding.inflate(LayoutInflater.from(context), parent, false)
        return JurusanViewHolder(binding)
    }

    class JurusanViewHolder (val binding: ItemJurusanBinding):
    RecyclerView.ViewHolder(binding.root)

    fun setArray(dList: List<Jurusan>) {
        this.arrlist = dList
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: JurusanViewHolder, position: Int) {
        val jurusan = arrlist.getOrNull(position)

        if (jurusan != null){
            val  picasso = Picasso.Builder(context)
                .build()

            picasso.load(getImageForJurusan(jurusan.idJurusan))
                .into(holder.binding.imgJurusan)

            holder.binding.tvJurusan.text = jurusan.jurusan
        }
    }

    private fun getImageForJurusan(idJurusan: String): Int {
        return when (idJurusan) {
            "1" -> R.drawable.rpl
            "2" -> R.drawable.tei
            "3" -> R.drawable.sija
            "4" -> R.drawable.toi
            "5" -> R.drawable.tek
            "6" -> R.drawable.iop
            "7" -> R.drawable.meka
            "8" -> R.drawable.tptup
            "9" -> R.drawable.pspt
            else -> R.drawable.bg_form
        }
    }

    override fun getItemCount(): Int {
        return arrlist!!.size
    }

}