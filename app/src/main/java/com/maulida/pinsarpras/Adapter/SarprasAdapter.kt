package com.maulida.pinsarpras.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maulida.pinsarpras.Model.Jurusan
import com.maulida.pinsarpras.Model.Sarpras
import com.maulida.pinsarpras.R
import com.maulida.pinsarpras.databinding.ItemJurusanBinding
import com.maulida.pinsarpras.databinding.ItemSarprasBinding
import com.squareup.picasso.Picasso

class SarprasAdapter(c: Context) : RecyclerView.Adapter<SarprasAdapter.SarprasViewHolder>()  {
    private var context: Context = c
    private var arrlist: List<Sarpras> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SarprasAdapter.SarprasViewHolder {
        val binding = ItemSarprasBinding.inflate(LayoutInflater.from(context), parent, false)
        return SarprasViewHolder(binding)
    }

    class SarprasViewHolder (val binding: ItemSarprasBinding):
        RecyclerView.ViewHolder(binding.root)

    fun setArray(dList: List<Sarpras>) {
        this.arrlist = dList
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: SarprasAdapter.SarprasViewHolder, position: Int) {
        val sarpras = arrlist.getOrNull(position)

        if (sarpras != null){
            val  picasso = Picasso.Builder(context)
                .build()

            picasso.load(getImageForSarpras(sarpras.idSarpras))
                .into(holder.binding.imgSarpras)


            holder.binding.ktgriSarpras.text = sarpras.kategori
            holder.binding.descSarpras.text = sarpras.desKategori
        }
    }

    private fun getImageForSarpras(idJurusan: String): Int {
        return when (idJurusan) {
            "1" -> R.drawable.sarana
            "2" -> R.drawable.prasarana
            else -> R.drawable.bg_form
        }
    }

    override fun getItemCount(): Int {
        return arrlist!!.size
    }

}