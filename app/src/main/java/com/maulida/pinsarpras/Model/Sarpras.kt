package com.maulida.pinsarpras.Model

class Sarpras (
    var idSarpras: String ="",
    var kategori: String ="",
    var desKategori: String =""
        )

val sarpras1 = Sarpras(
    idSarpras = "1",
    kategori = "SARANA",
    desKategori = "Sarana sekolah merujuk pada fasilitas fisik yang digunakan dalam proses pembelajaran dan operasi sekolah."
)

val sarpras2 = Sarpras(
    idSarpras = "2",
    kategori = "PRASARANA",
    desKategori = "Prasarana sekolah adalah fasilitas fisik yang mendukung operasi sekolah secara keseluruhan."
)

val listDataSarpras = listOf(sarpras1, sarpras2)