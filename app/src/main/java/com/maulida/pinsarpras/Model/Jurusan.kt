package com.maulida.pinsarpras.Model

class Jurusan (
    var idJurusan: String ="",
    var jurusan: String = ""
        )

val jurusan1 = Jurusan(
    idJurusan = "1",
    jurusan = "RPL"
)

val jurusan2 = Jurusan(
    idJurusan = "2",
    jurusan = "TEI"
)

val jurusan3 = Jurusan(
    idJurusan = "3",
    jurusan = "SIJA"
)

val jurusan4 = Jurusan(
    idJurusan = "4",
    jurusan = "TOI"
)

val jurusan5 = Jurusan(
    idJurusan = "5",
    jurusan = "TEK"
)

val jurusan6 = Jurusan(
    idJurusan = "6",
    jurusan = "IOP"
)

val jurusan7 = Jurusan(
    idJurusan = "7",
    jurusan = "MEKA"
)

val jurusan8 = Jurusan(
    idJurusan = "8",
    jurusan = "TPTUP"
)

val jurusan9 = Jurusan(
    idJurusan = "9",
    jurusan = "PSPT"
)

val listDataJurusan = listOf(jurusan1, jurusan2, jurusan3, jurusan4, jurusan5, jurusan6, jurusan7, jurusan8, jurusan9)
