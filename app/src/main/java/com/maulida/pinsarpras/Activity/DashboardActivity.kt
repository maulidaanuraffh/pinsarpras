package com.maulida.pinsarpras.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.maulida.pinsarpras.Adapter.JurusanAdapter
import com.maulida.pinsarpras.Adapter.SarprasAdapter
import com.maulida.pinsarpras.Model.Jurusan
import com.maulida.pinsarpras.Model.Sarpras
import com.maulida.pinsarpras.Model.listDataJurusan
import com.maulida.pinsarpras.Model.listDataSarpras
import com.maulida.pinsarpras.R
import com.maulida.pinsarpras.databinding.ActivityDashboardBinding

class DashboardActivity : AppCompatActivity() {
    private val TAG: String="DashboardActivity"
    private lateinit var jurusanAdapter: JurusanAdapter
    private lateinit var sarprasAdapter: SarprasAdapter
    private lateinit var binding: ActivityDashboardBinding

    private var listJurusan = ArrayList<Jurusan>()
    private var listSarpras = ArrayList<Sarpras>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding  = ActivityDashboardBinding.inflate(layoutInflater)
        setContentView(binding.root)

        listSarpras.addAll(listDataSarpras)
        listJurusan.addAll(listDataJurusan)
        sarprasAdapter = SarprasAdapter(this)
        jurusanAdapter = JurusanAdapter(this)

        binding.rvSarpras.layoutManager = LinearLayoutManager(this)
        sarprasAdapter.setArray(listSarpras)
        binding.rvSarpras.adapter = sarprasAdapter

        binding.rvJurusan.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        jurusanAdapter.setArray(listJurusan)
        binding.rvJurusan.adapter = jurusanAdapter
    }
}